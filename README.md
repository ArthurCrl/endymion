# Endymion

A simple cross-platform tasks & process orchestrator

## Why that name ?

[Endymion](https://en.wikipedia.org/wiki/Endymion_(mythology)) is a shepherd, leading its sheeps (a.k.a. tasks) to the right destination.
It is also a tribute to the top notch [series from Dan Simmons](https://en.wikipedia.org/wiki/Hyperion_Cantos)

## Why that icon ?

[Because its a nice butterfly](https://en.wikipedia.org/wiki/Turanana_endymion)
