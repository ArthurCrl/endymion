
export interface IConfigurationSchema {
  $schema?: string
  /**
   * The working directory where all the repo will be dupplicated
   */
  repoPath: string
  repositories: { [key: string]: IRepositoryConfiguration }
}

export interface IRepositoryConfiguration {
  /**
   * The branch to use for the current repo
   */
  branch?: string
  /**
   * The environments that can be lauched with current repo
   */
  environments: { [key: string]: IExecutionEnvironment }
  /**
   * The command to be launched to initialize the repo
   */
  initCommand?: string
  url?: string
}

/**
 * An environment to lauchn with a command, the definition of its readyness condition and
 * some optional dependencies
 */
export interface IExecutionEnvironment {
  currentPid?: number
  arguments?: string[]
  command?: string
  dependsOn?: string[]
  id?: string
  /**
   * The condition to assume the environment is ready
   */
  readyWhen?: IReadyWhen
  repo?: string,
  ready?: boolean
}

/**
 * The condition to assume the environment is ready
 *
 * The environment is ready when the port is up
 *
 * The environment is ready when the message (containing the port '${port}') is found
 */
export interface IReadyWhen {
  portIsUp?: number
  consoleMessageIsFound?: string
}

export enum EConfigRepositoryItemType {
  all = 'all',
  url = 'url',
  command = 'command',
  branch = 'branch',
  environments = 'environments'
}

export enum EConfigEnvironmentItemType {
  command = 'command',
  parameters = 'parameters',
  dependsOn = 'dependsOn',
  isReadyWhen = 'isReadyWhen',
}
