import envs from '@/env'
import { createLogger, format, transports } from 'winston'
const logger = createLogger({
  level: process.env.LOG_LEVEL || 'error',
  format: format.combine(
    format.timestamp(),
    format.align(),
    format.printf((info) => `[${info.timestamp}][${info.level}] ${info.message}`)
  ),
  transports: [
    //
    // - Write to all logs with level `info` and below to `quick-start-combined.log`.
    // - Write all logs error (and below) to `quick-start-error.log`.
    //
    new transports.File({ filename: process.env.LOG_ERROR_FILE || 'error.log', level: 'error' }),
    new transports.File({ filename: process.env.LOG_FILE || 'trace.log' })
  ]
})

//
// If we're not in production then **ALSO** log to the `console`
// with the colorized simple format.
//
if (process.env.NODE_ENV !== 'production') {
  logger.add(new transports.Console({
    format: format.combine(
      format.colorize(),
      format.timestamp(),
      format.align(),
      format.printf((info) => `[${info.timestamp}][${info.level}] ${info.message}`)
    )
  }))
  logger.debug(`Loaded env variables: ${JSON.stringify(envs.parsed)}`)
}

export default logger
