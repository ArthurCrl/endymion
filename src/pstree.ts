'use strict'
import logger from '@/logger'
import child_process from 'child_process'
const spawn = child_process.spawnSync

export default function childrenOfPid(
  pid: string | number): Array<{}> {
  try {
    let headers: any[]

    if (typeof pid === 'number') {
      pid = pid.toString()
    }

    //
    // The `ps-tree` module behaves differently on *nix vs. Windows
    // by spawning different programs and parsing their output.
    //
    // Linux:
    // 1. " <defunct> " need to be striped
    // ```bash
    // $ ps -A -o comm,ppid,pid,stat
    // COMMAND          PPID   PID STAT
    // bbsd             2899 16958 Ss
    // watch <defunct>  1914 16964 Z
    // ps              20688 16965 R+
    // ```
    //
    // Win32:
    // 1. wmic PROCESS WHERE ParentProcessId=4604 GET Name,ParentProcessId,ProcessId,Status)
    // 2. The order of head columns is fixed
    // ```shell
    // > wmic PROCESS GET Name,ProcessId,ParentProcessId,Status
    // Name                          ParentProcessId  ProcessId   Status
    // System Idle Process           0                0
    // System                        0                4
    // smss.exe                      4                228
    // ```

    let processes
    if (process.platform === 'win32') {
      // See also: https://github.com/nodejs/node-v0.x-archive/issues/2318
      processes = spawn('wmic.exe', [
        'PROCESS',
        'GET',
        'Name,ProcessId,ParentProcessId,Status'
      ]).output.toString()
    } else {
      processes = spawn('ps', [
        '-A',
        '-o',
        'ppid,pid,stat,comm'
      ]).output.toString()
    }
    processes = processes.substring(2).split('\n').map((line) => {
      const columns = line
        .toString()
        .trim()
        .split(/\s+/)
      try {
        if (!headers) {
          headers = columns

          //
          // Rename Win32 header name, to as same as the linux, for compatible.
          //
          headers = headers.map(normalizeHeader)
        }

        const row: any = {}
        // For each header
        const h = headers.slice()
        while (h.length) {
          row[h.shift()] = h.length ? columns.shift() : columns.join(' ')
        }
        return row
      } catch (e) {
        logger.error('Error: ', e)
      }
    })

    const parents: any = {}
    const children: any[] = []
    parents[pid] = true
    processes.forEach((proc: any) => {
      if (parents[proc.PPID]) {
        parents[proc.PID] = true
        children.push(proc)
      }
    })
    return children
  } catch (e) {
    return []
  }
}

/**
 * Normalizes the given header `str` from the Windows
 * title to the *nix title.
 *
 * @param {string} str Header string to normalize
 */
function normalizeHeader(str: string) {
  if (process.platform !== 'win32') {
    return str
  }

  switch (str) {
    case 'Name':
      return 'COMMAND'
    case 'ParentProcessId':
      return 'PPID'
    case 'ProcessId':
      return 'PID'
    case 'Status':
      return 'STAT'
    default:
      throw new Error('Unknown process listing header: ' + str)
  }
}
